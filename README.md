#XMindCrack

XMind 破解工具
测试 [XMind7.5](http://www.xmind.net/xmind/downloads/xmind-7.5-update1-windows.exe)、[XMind8](https://www.xmind.net/xmind/downloads/xmind-8-windows.exe)、[XMind8 Update1](https://www.xmind.net/xmind/downloads/xmind-8-update1-windows.exe) 通过

使用步骤:

1. 将 ``XMindCrack.jar`` 复制到 D盘根目录.
2. 找到 ``XMind`` 安装目录, 如: ``D:\Program Files (x86)\XMind``
3. 以文本格式打开安装目录中 ``XMind.ini``
4. 在 ``XMind.ini`` 最后追加 ``-javaagent:D:/XMindCrack.jar``
5. 打开 XMind, 输入序列号 ``XAka34A2rVRYJ4XBIU35UZMUEEF64CMMIYZCK2FZZUQNODEKUHGJLFMSLIQMQUCUBXRENLK6NZL37JXP4PZXQFILMQ2RG5R7G4QNDO3PSOEUBOCDRYSSXZGRARV6MGA33TN2AMUBHEL4FXMWYTTJDEINJXUAV4BAYKBDCZQWVF3LWYXSDCXY546U3NBGOI3ZPAP2SO3CSQFNB7VVIY123456789012345``激活.

